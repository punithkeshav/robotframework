*** Settings ***
Library  SeleniumLibrary
Library  SeleniumLibrary

*** Variables ***
${Browser}  headlesschrome
${url}  https://project-anglo.isometrix.net/login.aspx?ReturnUrl=%2f

*** Test Cases ***
LoginTest
    Open Browser    ${url}     ${Browser}
    maximize browser window
    loginAngloProjectSite
    AuditManagementModule
    Close Browser



*** Keywords ***
loginAngloProjectSite
    ${"UserName"}   set variable    id:txtUsername
    element should be visible   ${"UserName"}
    element should be enabled   ${"UserName"}

    input text  ${"UserName"}  Admin
    set selenium speed      1seconds

    ${"Password"}   set variable    id:txtPassword
    element should be visible   ${"Password"}
    element should be enabled   ${"Password"}

    input text  ${"Password"}  123
    set selenium speed      1seconds

    ${"SignIn"}     set variable    id:btnLoginSubmit
    click element   ${"SignIn"}

    title should be     IsoMetrix
     set selenium speed      5seconds

AuditManagementModule
    Sleep   5
    Select Frame    xpath://iframe[@id='ifrMain']
    set selenium speed      5seconds
    ${"Audit Management"}     set variable    xpath://div[@original-title='Audit Management']
    # ${"Audit Management"}     set variable    id:section_937a16a1-5130-4da5-8c5c-7f99059a9f6b
    click element   ${"Audit Management"}
    set selenium speed      5seconds

    ${"Add Audit Management"}     set variable    xpath://div[text()='Add']
    click element   ${"Add Audit Management"}
    set selenium speed      2seconds

    ${"Process Flow"}     set variable    xpath://span[@i18n='process_flow']
    click element   ${"Process Flow"}
    set selenium speed      2seconds




    #Selecting checkBox --Select checkbox
    #Selecting Radio Buttons -- select radio button



