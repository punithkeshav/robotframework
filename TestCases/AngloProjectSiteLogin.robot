*** Settings ***
Library  SeleniumLibrary
Library  SeleniumLibrary
Library  SeleniumLibrary

*** Variables ***
${Browser}  headlesschrome
${url}  https://project-anglo.isometrix.net/login.aspx?ReturnUrl=%2f

*** Test Cases ***
LoginTest
    Open Browser    ${url}     ${Browser}
    loginAngloProjectSite
    close Browser



*** Keywords ***
loginAngloProjectSite
    input text  id:txtUsername  Admin
    input text  id:txtPassword  123
    click element   id:btnLoginSubmit
    title should be     IsoMetrix
